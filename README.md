[![pipeline status](https://gitlab.com/grunlab/deluge/badges/main/pipeline.svg)](https://gitlab.com/grunlab/deluge/-/commits/main)

# GrunLab Deluge

Deluge non-root container images (deluged, deluge-web) and deployment on Kubernetes.

Docs:
- https://docs.grunlab.net/images/deluge.md
- https://docs.grunlab.net/apps/deluge.md

Base image: [grunlab/base-image/ubuntu:22.04][base-image]

Format: docker

Supported architecture(s):
- arm64

[base-image]: <https://gitlab.com/grunlab/base-image>